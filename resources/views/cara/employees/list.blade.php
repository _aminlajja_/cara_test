@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h1>Employee Page</h1>
                     <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#employeesModal">
                         Add
                     </button>
                     {{-- {{auth()->user()->checkAccess()}} --}}
                 </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center pt-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">List Of Employees</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered user_datatable">
                            <thead>
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        First Name
                                    </th>
                                    <th>
                                        Last Name
                                    </th>
                                    <th>
                                        Company
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($list as $employees)
                                    <tr>
                                        <td>
                                            {{$list->firstItem() + $loop->index}}
                                        </td>
                                        <td>
                                            {{$employees->firstname}}
                                        </td>
                                        <td>
                                            {{$employees->lastname}}
                                        </td>
                                        <td>
                                            {{$employees->company}}
                                        </td>
                                        <td>
                                            {{$employees->email}}
                                        </td>
                                        <td>
                                            {{$employees->phone}}
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="6">No Record</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        {{-- <div class="col-md-12">
                            {{ $list->links('vendor.pagination.bootstrap-5')}}
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="employeesModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action=""></form>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Employees Detail</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="fromEmployees" action="{{ route('employees.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="firstname" class="col-md-3 col-form-label text-md-right">First Name</label>
                            <div class="col-md-7">
                                <input id="firstname" type="firstname" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" autocomplete="firstname" autofocus required>
                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-3 col-form-label text-md-right">Last Name</label>
                            <div class="col-md-7">
                                <input id="lastname" type="lastname" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" autocomplete="lastname" autofocus required>
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="company" class="col-md-3 col-form-label text-md-right">Company</label>
                            <div class="col-md-7">
                                <input id="company" type="company" class="form-control @error('company') is-invalid @enderror" name="company" value="{{ old('company') }}" autocomplete="company" autofocus required>
                                @error('company')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right">Email</label>
                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="phone" class="col-md-3 col-form-label text-md-right">Phone No.</label>
                            <div class="col-md-7">
                                <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autocomplete="phone" autofocus required>
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script> --}}
<script type="text/javascript">

    function deleteData(id){
        window.location.href = '{{ route('employees.delete') }}?id='+id;
    }

    function edit(id){
        console.log(id);
        let data = {};

        $.get("{{route('employees.show')}}", {
                id: id
            }, function(result){
                data = result;
                console.log(result);
                let url_update = '{{ route('employees.update') }}';
                let id = data.id;
                let firstname = data.firstname;
                let lastname = data.lastname;
                let email = data.email;
                let company = data.company;
                let phone = data.phone;

                let modalEmployees = $('#employeesModal');
                let fromEmployees = $('#fromEmployees');
                let action = document.getElementById("fromEmployees").action = url_update;
                console.log(action);

                modalEmployees.find('#firstname').val(firstname);
                modalEmployees.find('#lastname').val(lastname);
                modalEmployees.find('#company').val(company);
                modalEmployees.find('#email').val(email);
                modalEmployees.find('#phone').val(phone);
                fromEmployees.append(' <input name="id" id="" type="hidden" value='+id+'> ');

                modalEmployees.modal('show');
            });


    }

    $(function () {
        var table = $('.user_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('employees.getEmployees') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'firstname', name: 'firstname'},
                {data: 'lastname', name: 'lastname'},
                {data: 'company', name: 'company'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });

</script>
@endsection
