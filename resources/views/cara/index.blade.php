@extends('layouts.app')

@section('content')
<div class="container">
    {{-- @json($notification_list) --}}
    @auth
        @if(auth()->user()->hasUsersTypes->code == '02')
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Makluman</div>

                        <div class="card-body">
                            @forelse ($notification_list as $notify)
                                <h3>{{ $notify->hasNotication['title'] }}</h3>
                                <p>
                                    {{ $notify->hasNotication['message'] }}
                                </p>
                                <p>
                                    <a class="btn btn-info" href="{{$notify->hasNotication['target_url']}}">Pautan</a>
                                </p>
                                <hr soft/>
                            @empty

                            @endforelse

                            <div class="col-md-12">
                                {{ $notification_list->links('vendor.pagination.bootstrap-5')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <br>
    @endauth
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @auth
                        {{ __('You are logged in!') }}
                    @else
                        Welcome to Cara HR
                    @endauth
                    {{-- {{auth()->user()->checkAccess()}} --}}
                    {{-- {{ public_path() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
