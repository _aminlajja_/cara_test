@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                   <h1>Company Page</h1>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#companiesModal">
                        Add
                    </button>
                    {{-- {{auth()->user()->checkAccess()}} --}}
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center pt-2">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List Of Companies</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed user_datatable">
                            <thead>
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Logo
                                    </th>
                                    <th>
                                        Website
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($list as $company)
                                    <tr>
                                        <td>
                                            {{$list->firstItem() + $loop->index}}
                                        </td>
                                        <td>
                                            {{$company->name}}
                                        </td>
                                        <td>
                                            @php
                                                $path = env('APP_URL')."public/storage".$company->logo
                                            @endphp
                                            <img src="{{$path}}" alt="{{$company->logo}}"/>
                                        </td>
                                        <td>
                                            {{$company->website}}
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{ route('company.delete', ['id' => $company->id] ) }}">
                                                    Delete
                                                </a>
                                                <a class="btn btn-info" onclick="edit({{json_encode($company)}});">
                                                    Update
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="5">No Record</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="col-md-12">
                            {{ $list->links('vendor.pagination.bootstrap-5')}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="companiesModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action=""></form>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Company Detail</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="fromCompany" action="{{ route('company.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">Name</label>
                            <div class="col-md-7">
                                <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus required>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right">Email</label>
                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="logo" class="col-md-3 col-form-label text-md-right">Upload Logo</label>
                            <div class="col-md-7">
                                <input type="file" name="logo" id="logo" class="hidden" />
                                @error('logo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="website" class="col-md-3 col-form-label text-md-right">Website (Url)</label>
                            <div class="col-md-7">
                                <input id="website" type="text" class="form-control @error('website') is-invalid @enderror" name="website" value="{{ old('website') }}" autocomplete="website" autofocus required>
                                @error('website')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    function edit(data){
        // console.log(data);

        let url_update = '{{ route('company.update') }}';
        let id = data.id;
        let name = data.name;
        let email = data.email;
        let logo = data.logo;
        let website = data.website;

        let modalCompany = $('#companiesModal');
        let fromCompany = $('#fromCompany');
        let action = document.getElementById("fromCompany").action = url_update;
        // console.log(action);

        modalCompany.find('#name').val(name);
        modalCompany.find('#email').val(email);
        modalCompany.find('#name').val(name);
        modalCompany.find('#website').val(website);
        fromCompany.append(' <input name="id" id="" type="hidden" value='+id+'> ');

        modalCompany.modal('show');
    }
</script>
@endsection
