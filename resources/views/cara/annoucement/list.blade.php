@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h1>Announcement Page</h1>
                     <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#announcementModal">
                         Add
                     </button>
                     {{-- {{auth()->user()->checkAccess()}} --}}
                 </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center pt-2">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List Of Announcement</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered data-table yajra-datatable">
                            <thead>
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th>
                                        Image
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($list as $announcement)
                                    <tr>
                                        <td>
                                            {{$list->firstItem() + $loop->index}}
                                        </td>
                                        <td>
                                            {{$announcement->title}}
                                        </td>
                                        <td>
                                            {{$announcement->description}}
                                        </td>
                                        <td>
                                            @php
                                                $path = public_path()."/storage".$announcement->image;
                                            @endphp
                                            <img src="{{Storage::url("/app/".$announcement->image)}}" alt="{{$announcement->image}}"/>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-danger" href="{{ route('announcement.delete', ['id' => $announcement->id] ) }}">
                                                    Delete
                                                </a>
                                                <a class="btn btn-info" onclick="edit({{json_encode($announcement)}});">
                                                    Update
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="5">No Record</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                        <div class="col-md-12">
                            {{ $list->links('vendor.pagination.bootstrap-5')}}
                        </div>
                        {{-- <div class="pagination">{{$list->links()}}</div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="announcementModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action=""></form>
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Announcement Detail</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form method="POST" id="fromAnnouncement" action="{{ route('announcement.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="col-md-3 col-form-label text-md-right">Title</label>
                            <div class="col-md-7">
                                <input id="title" type="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" autocomplete="title" autofocus required>
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="description" class="col-md-3 col-form-label text-md-right">Description</label>
                            <div class="col-md-7">
                                {{-- <input id="description" type="description" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" autocomplete="description" autofocus required> --}}
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="descriptionannouncement" cols="30" rows="5"></textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row">
                            <label for="image" class="col-md-3 col-form-label text-md-right">Upload Image</label>
                            <div class="col-md-7">
                                <input type="file" name="image" id="image" class="hidden" />
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <br>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    function edit(data){
        // console.log(data);

        let url_update = '{{ route('announcement.update') }}';
        let id = data.id;
        let title = data.title;
        let description = data.description;

        let modalAnnouncement = $('#announcementModal');
        let fromAnnouncement = $('#fromAnnouncement');
        let action = document.getElementById("fromAnnouncement").action = url_update;
        // console.log(action);

        modalAnnouncement.find('#title').val(title);
        modalAnnouncement.find('#description').val(description);
        fromAnnouncement.append(' <input name="id" id="" type="hidden" value='+id+'> ');

        modalAnnouncement.modal('show');
    }

</script>
@endsection
