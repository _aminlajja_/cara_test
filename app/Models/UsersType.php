<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
class UsersType extends Model
{
    use HasFactory;
    use HasJsonRelationships;

    protected $table = 'users_types';

    protected $fillabled = [
        'code',
        'description',
        'access_all',
        'menu_to_display',
    ];

    protected $casts = [
        'menu_to_display' => 'json',
    ];

    public function hasMenuAccess(){
        return $this->belongsToJson(Menu::class, 'menu_to_display');
    }

}
