<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MapUsersNotification extends Model
{
    use HasFactory;

    protected $table = 'map_users_notifications';

    protected $fillable = [
        'user_id',
        'status',
        'view_dt',
        'notification_id',
        'created_*',
        'updated_*',
    ];


    public function hasNotication(){
        return $this->belongsTo(Notification::class, 'notification_id');
    }
}
