<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $this->validate($request ,[
            'email' => 'required',
            // 'password' => 'required|confirmed',
            'password' => 'required',
        ], [
            'email.required' => 'Please fill in your email',
            'password.required' => 'Please key-in your password',
        ]);
        // dd($request->all());
        // auth()->attempt()->only('email', 'password');

        // Auth::attempt($request->only['email', 'password']) // user rgeister funciton together

        // auth()->attempt([
        //     'email' => $request->email,
        //     'password' => $request->password,
        // ]);
        Auth::attempt(['email' => $request->email, 'password' => $request->password]);
        // if(Auth::user()){
        return redirect('home');

        // return $this->guard()->attempt(
        //     $this->credentials($request), $request->boolean('remember')
        // );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $this->middleware('guest')->except('logout');
        Auth::logout();
        return redirect('home');
    }
}
