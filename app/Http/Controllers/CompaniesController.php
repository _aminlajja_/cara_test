<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Companies::paginate(5);
        return view('cara.companies.list', ['list' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'logo' => 'required',
            'email' => 'required|email',
            'website' => 'required',
        ]);
        // dd($request->all());
        $path = '/companies/';

        $file = $request->file('logo');

        $docFormat = $file->getClientOriginalExtension();
        $fileName = Str::random(9).'.'.$docFormat;

        if($file != null){
            Log::info($file);

            $file->storeAs('public/'.$path, $fileName);
            Log::info($fileName);
            // $path = Storage::putFile($path, $fileName);
        }

        $Companies = Companies::insert([
            'name' => $request->name,
            'email' => $request->email,
            'logo' => $path.$fileName,
            'website' => $request->website,
            'created_at' => Carbon::now(),
        ]);

        return back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $Companies = Companies::find($request->id);
        $data = [
            'name' => $request->name,
            'email' => $request->email,

            'website' => $request->website,
            'updated_at' => Carbon::now(),
        ];
        if($request->file('logo')){
            $path = '/companies/';

            $file = $request->file('logo');

            $docFormat = $file->getClientOriginalExtension();
            $fileName = Str::random(9).'.'.$docFormat;
            Log::info($file);

            $file->storeAs('public/'.$path, $fileName);
            Log::info($fileName);
            // $path = Storage::putFile($path, $fileName);
            $data['logo'] = $path.$fileName;
        }

        $Companies->update($data);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $Companies = Companies::find($id);
        if($Companies->logo != null){
            $deleteImage = unlink(public_path()."/storage".$Companies->logo);
        }
        $Companies->delete();

        return back();
    }
}
