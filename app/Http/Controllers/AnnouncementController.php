<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\MapUsersNotification;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AnnouncementController extends Controller
{
    public function index(){
        $list = Announcement::paginate(5);
        return view('cara.annoucement.list', ['list' => $list]);
    }

    public function create(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);
        // dd($request->all());
        $path = '/announcement/';

        $file = $request->file('image');

        $docFormat = $file->getClientOriginalExtension();
        $fileName = Str::random(9).'.'.$docFormat;

        if($file != null){
            Log::info($file);

            $file->storeAs('public/'.$path, $fileName);
            Log::info($fileName);
        }

        $Announcement = Announcement::insert([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $path.$fileName,
            'created_at' => Carbon::now(),
        ]);

        $User = User::whereHas('hasUsersTypes', function($q){
                        $q->where('code', '02');
                    })->get();

        $Notification = Notification::create([
            'title' => $request->title,
            'message' => 'Sila lihat maklumat ini',
            'target_url' => route('employees.list'),
            'created_at' => Carbon::now(),
        ]);

        foreach ($User as $users) {
            $MapUsersNotification = MapUsersNotification::create([
                'user_id' => $users->id,
                'notification_id' => $Notification->id,
            ]);
        }

        return back();

    }

    public function update(Request $request)
    {
        $Announcement = Announcement::find($request->id);
        $data = [
            'title' => $request->title,
            'description' => $request->description,
            'updated_at' => Carbon::now(),
        ];
        if($request->file('image')){
            $path = '/announcement/';

            $file = $request->file('image');

            $docFormat = $file->getClientOriginalExtension();
            $fileName = Str::random(9).'.'.$docFormat;
            Log::info($file);

            $file->storeAs('public/'.$path, $fileName);
            Log::info($fileName);
            // $path = Storage::putFile($path, $fileName);
            $data['image'] = $path.$fileName;
        }

        $Announcement->update($data);

        return back();
    }

    public function destroy(Request $request)
    {
        $Announcement = Announcement::find($request->id);
        if($Announcement->image){
            $deleteImage = unlink(public_path()."/storage".$Announcement->image);
        }
        $Announcement->delete();

        return back();
    }
}
