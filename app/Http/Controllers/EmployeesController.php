<?php

namespace App\Http\Controllers;

use App\Models\Employees;
use App\Models\User;
use App\Models\UsersType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Employees::paginate(5);
        // $has_view =
        return view('cara.employees.list', ['list' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'company' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric|digits:10',
        ]);
        // dd($request->all());

        $data = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'company' => $request->company,
            'email' => $request->email,
            'phone' => $request->phone,
            'created_at' => Carbon::now(),
        ];

        $Employees = Employees::insert($data);

        $User = User::create([
            'name' => $request->firstname.' '.$request->lastname,
            'email' => $request->email,
            'password' => Hash::make('password'),
            'role_id' => UsersType::where('code', '02')->first()->id,
            'created_at' => Carbon::now(),
        ]);

        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        Log::info($request->all());
        $Employees = Employees::find($request->id);

        return $Employees;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function edit(Employees $employees)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employees $employees)
    {
        $Employees = Employees::find($request->id);
        $data = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'company' => $request->company,
            'email' => $request->email,
            'phone' => $request->phone,
            'updated_at' => Carbon::now(),
        ];

        $Employees->update($data);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employees  $employees
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Employees = Employees::find($request->id);
        $Employees->delete();

        return back();
    }


    public function getEmployees(Request $request)
    {
        if ($request->ajax()) {

            $data = Employees::latest()->get();
            return Datatables::of($data)->addIndexColumn()
                ->addColumn('action', function($row){
                        $btn = '';
                        // $btn = '<div class="btn-group"><a class="btn btn-danger" href="{{route("employees.delete")}}">Delete</a><a class="btn btn-info" onclick="edit({{json_encode($employees)}});">Update</a></div>';
                        // $btn = '<div class="btn-group"><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row['id'].'" data-original-title="Edit" class="edit btn btn-danger">Delete</a>';
                        // $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="" data-original-title="Delete" class="btn btn-info">Update</a></div>';
                        $btn = '<div class="btn-group"> <a data-toggle="tooltip"  data-id="'.$row['id'].'" data-original-title="Edit" class="btn btn-danger" onclick="deleteData('.$row['id'].')">Delete</a> <a class="btn btn-info" onclick="edit('.$row['id'].')">Update</a></div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }


}
