<?php

namespace App\Http\Controllers;

use App\Models\MapUsersNotification;
use Illuminate\Http\Request;

class MapUsersNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MapUsersNotification  $mapUsersNotification
     * @return \Illuminate\Http\Response
     */
    public function show(MapUsersNotification $mapUsersNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MapUsersNotification  $mapUsersNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(MapUsersNotification $mapUsersNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MapUsersNotification  $mapUsersNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MapUsersNotification $mapUsersNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MapUsersNotification  $mapUsersNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(MapUsersNotification $mapUsersNotification)
    {
        //
    }
}
