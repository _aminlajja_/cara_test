<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('menus')->delete();

        \DB::table('menus')->insert(array (
            0 =>
            array (
                'code' => '01',
                'menu_name' => 'Companies',
                'url' => route('company.list'),
                'created_at' => '2023-01-27 17:06:18',
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'code' => '02',
                'menu_name' => 'Employees',
                'url' => route('employees.list'),
                'created_at' => '2023-01-27 17:06:37',
                'updated_at' => NULL,
            ),
            2 =>
            array (
                'code' => '03',
                'menu_name' => 'Announcement',
                'url' => route('announcement.list'),
                'created_at' => '2023-01-27 17:06:55',
                'updated_at' => NULL,
            ),
        ));


    }
}
