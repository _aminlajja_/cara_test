<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users_types')->delete();

        \DB::table('users_types')->insert(array (
            0 =>
            array (
                'code' => '01',
                'description' => 'Admin',
                'access_all' => 1,
                'menu_to_display' => '[1, 2, 3]',
                'created_at' => '2023-01-27 15:51:26',
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'code' => '02',
                'description' => 'Employee',
                'access_all' => 0,
                'menu_to_display' => '[3]',
                'created_at' => '2023-01-27 15:51:45',
                'updated_at' => NULL,
            ),
        ));


    }
}
