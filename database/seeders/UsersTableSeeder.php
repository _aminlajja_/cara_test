<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert(array (
            0 =>
            array (
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => Hash::make('password'),
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'role_id' => 1,
            ),
            1 =>
            array (
                'name' => 'Employee1',
                'email' => 'employee1@employee.com',
                'email_verified_at' => NULL,
                'password' => Hash::make('password'),
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'role_id' => 2,
            ),
            2 =>
            array (
                'name' => 'Employee2',
                'email' => 'employee2@employee.com',
                'email_verified_at' => NULL,
                'password' => Hash::make('password'),
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'role_id' => 2,
            ),
        ));
    }
}
