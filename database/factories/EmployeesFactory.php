<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employees>
 */
class EmployeesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'firstname' => fake()->name(),
            'lastname' => fake()->name(),
            'company' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'phone' => '0145675671',
        ];
    }
}
