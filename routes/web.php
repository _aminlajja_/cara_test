<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
    // return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeIndexController::class, 'index'])->name('home');

Route::get('/login', [App\Http\Controllers\LoginController::class, 'index'])->name('login');
Route::post('/login', [App\Http\Controllers\LoginController::class, 'create']);
Route::post('/logout', [App\Http\Controllers\LoginController::class, 'destroy'])->name('logout');

Route::group(['prefix' => 'company', 'as' => 'company', 'middleware' => 'auth'], function(){

    Route::get('/list', [App\Http\Controllers\CompaniesController::class, 'index'])->name('.list');
    Route::post('/store{id?}', [App\Http\Controllers\CompaniesController::class, 'create'])->name('.store');
    Route::get('/delete{id?}', [App\Http\Controllers\CompaniesController::class, 'destroy'])->name('.delete');
    Route::post('/update{id?}', [App\Http\Controllers\CompaniesController::class, 'update'])->name('.update');

});

Route::group(['prefix' => 'employees', 'as' => 'employees', 'middleware' => 'auth'], function(){

    Route::get('/list', [App\Http\Controllers\EmployeesController::class, 'index'])->name('.list');
    Route::post('/store{id?}', [App\Http\Controllers\EmployeesController::class, 'create'])->name('.store');
    Route::get('/delete{id?}', [App\Http\Controllers\EmployeesController::class, 'destroy'])->name('.delete');
    Route::post('/update{id?}', [App\Http\Controllers\EmployeesController::class, 'update'])->name('.update');
    Route::get('/show', [App\Http\Controllers\EmployeesController::class, 'show'])->name('.show');
    Route::get('/getEmployees', [App\Http\Controllers\EmployeesController::class, 'getEmployees'])->name('.getEmployees');

});

Route::group(['prefix' => 'announcement', 'as' => 'announcement', 'middleware' => 'auth'], function(){

    Route::get('/list', [App\Http\Controllers\AnnouncementController::class, 'index'])->name('.list');
    Route::post('/store{id?}', [App\Http\Controllers\AnnouncementController::class, 'create'])->name('.store');
    Route::get('/delete{id?}', [App\Http\Controllers\AnnouncementController::class, 'destroy'])->name('.delete');
    Route::post('/update{id?}', [App\Http\Controllers\AnnouncementController::class, 'update'])->name('.update');
    Route::get('/getAnnouncement', [App\Http\Controllers\AnnouncementController::class, 'getAnnouncement'])->name('.getAnnouncement');

});
